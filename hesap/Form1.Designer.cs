﻿namespace hesap
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ekranLabel = new System.Windows.Forms.Label();
            this.sayi1button = new System.Windows.Forms.Button();
            this.sayi2button = new System.Windows.Forms.Button();
            this.sayi3button = new System.Windows.Forms.Button();
            this.sayiartibutton = new System.Windows.Forms.Button();
            this.sayieksibutton = new System.Windows.Forms.Button();
            this.sayi6button = new System.Windows.Forms.Button();
            this.sayi5button = new System.Windows.Forms.Button();
            this.sayi4button = new System.Windows.Forms.Button();
            this.sayicarpibutton = new System.Windows.Forms.Button();
            this.sayi9button = new System.Windows.Forms.Button();
            this.sayi8button = new System.Windows.Forms.Button();
            this.sayi7button = new System.Windows.Forms.Button();
            this.sayibolubutton = new System.Windows.Forms.Button();
            this.sayiesitbutton = new System.Windows.Forms.Button();
            this.sayi0button = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ekranLabel
            // 
            this.ekranLabel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ekranLabel.Enabled = false;
            this.ekranLabel.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ekranLabel.ForeColor = System.Drawing.Color.CadetBlue;
            this.ekranLabel.Location = new System.Drawing.Point(59, 45);
            this.ekranLabel.Name = "ekranLabel";
            this.ekranLabel.Size = new System.Drawing.Size(361, 69);
            this.ekranLabel.TabIndex = 0;
            this.ekranLabel.Text = "0";
            this.ekranLabel.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // sayi1button
            // 
            this.sayi1button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.sayi1button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sayi1button.ForeColor = System.Drawing.Color.Crimson;
            this.sayi1button.Location = new System.Drawing.Point(59, 138);
            this.sayi1button.Name = "sayi1button";
            this.sayi1button.Size = new System.Drawing.Size(75, 70);
            this.sayi1button.TabIndex = 1;
            this.sayi1button.Text = "1";
            this.sayi1button.UseVisualStyleBackColor = false;
            this.sayi1button.Click += new System.EventHandler(this.button1_Click);
            // 
            // sayi2button
            // 
            this.sayi2button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.sayi2button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sayi2button.ForeColor = System.Drawing.Color.Crimson;
            this.sayi2button.Location = new System.Drawing.Point(157, 138);
            this.sayi2button.Name = "sayi2button";
            this.sayi2button.Size = new System.Drawing.Size(75, 70);
            this.sayi2button.TabIndex = 2;
            this.sayi2button.Text = "2";
            this.sayi2button.UseVisualStyleBackColor = false;
            this.sayi2button.Click += new System.EventHandler(this.button2_Click);
            // 
            // sayi3button
            // 
            this.sayi3button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.sayi3button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sayi3button.ForeColor = System.Drawing.Color.Crimson;
            this.sayi3button.Location = new System.Drawing.Point(255, 138);
            this.sayi3button.Name = "sayi3button";
            this.sayi3button.Size = new System.Drawing.Size(75, 70);
            this.sayi3button.TabIndex = 3;
            this.sayi3button.Text = "3";
            this.sayi3button.UseVisualStyleBackColor = false;
            this.sayi3button.EnabledChanged += new System.EventHandler(this.button3_Click);
            this.sayi3button.Click += new System.EventHandler(this.button3_Click);
            // 
            // sayiartibutton
            // 
            this.sayiartibutton.BackColor = System.Drawing.Color.LightCoral;
            this.sayiartibutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sayiartibutton.ForeColor = System.Drawing.Color.Crimson;
            this.sayiartibutton.Location = new System.Drawing.Point(346, 138);
            this.sayiartibutton.Name = "sayiartibutton";
            this.sayiartibutton.Size = new System.Drawing.Size(75, 70);
            this.sayiartibutton.TabIndex = 6;
            this.sayiartibutton.Text = "+";
            this.sayiartibutton.UseVisualStyleBackColor = false;
            // 
            // sayieksibutton
            // 
            this.sayieksibutton.BackColor = System.Drawing.Color.LightCoral;
            this.sayieksibutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sayieksibutton.ForeColor = System.Drawing.Color.Crimson;
            this.sayieksibutton.Location = new System.Drawing.Point(346, 218);
            this.sayieksibutton.Name = "sayieksibutton";
            this.sayieksibutton.Size = new System.Drawing.Size(75, 70);
            this.sayieksibutton.TabIndex = 10;
            this.sayieksibutton.Text = "-";
            this.sayieksibutton.UseVisualStyleBackColor = false;
            // 
            // sayi6button
            // 
            this.sayi6button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.sayi6button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sayi6button.ForeColor = System.Drawing.Color.Crimson;
            this.sayi6button.Location = new System.Drawing.Point(255, 218);
            this.sayi6button.Name = "sayi6button";
            this.sayi6button.Size = new System.Drawing.Size(75, 70);
            this.sayi6button.TabIndex = 9;
            this.sayi6button.Text = "6";
            this.sayi6button.UseVisualStyleBackColor = false;
            this.sayi6button.Click += new System.EventHandler(this.button6_Click);
            // 
            // sayi5button
            // 
            this.sayi5button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.sayi5button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sayi5button.ForeColor = System.Drawing.Color.Crimson;
            this.sayi5button.Location = new System.Drawing.Point(157, 218);
            this.sayi5button.Name = "sayi5button";
            this.sayi5button.Size = new System.Drawing.Size(75, 70);
            this.sayi5button.TabIndex = 8;
            this.sayi5button.Text = "5";
            this.sayi5button.UseVisualStyleBackColor = false;
            this.sayi5button.Click += new System.EventHandler(this.button7_Click);
            // 
            // sayi4button
            // 
            this.sayi4button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.sayi4button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sayi4button.ForeColor = System.Drawing.Color.Crimson;
            this.sayi4button.Location = new System.Drawing.Point(59, 218);
            this.sayi4button.Name = "sayi4button";
            this.sayi4button.Size = new System.Drawing.Size(75, 70);
            this.sayi4button.TabIndex = 7;
            this.sayi4button.Text = "4";
            this.sayi4button.UseVisualStyleBackColor = false;
            this.sayi4button.Click += new System.EventHandler(this.button8_Click);
            // 
            // sayicarpibutton
            // 
            this.sayicarpibutton.BackColor = System.Drawing.Color.LightCoral;
            this.sayicarpibutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sayicarpibutton.ForeColor = System.Drawing.Color.Crimson;
            this.sayicarpibutton.Location = new System.Drawing.Point(346, 294);
            this.sayicarpibutton.Name = "sayicarpibutton";
            this.sayicarpibutton.Size = new System.Drawing.Size(75, 70);
            this.sayicarpibutton.TabIndex = 14;
            this.sayicarpibutton.Text = "x";
            this.sayicarpibutton.UseVisualStyleBackColor = false;
            // 
            // sayi9button
            // 
            this.sayi9button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.sayi9button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sayi9button.ForeColor = System.Drawing.Color.Crimson;
            this.sayi9button.Location = new System.Drawing.Point(255, 294);
            this.sayi9button.Name = "sayi9button";
            this.sayi9button.Size = new System.Drawing.Size(75, 70);
            this.sayi9button.TabIndex = 13;
            this.sayi9button.Text = "9";
            this.sayi9button.UseVisualStyleBackColor = false;
            this.sayi9button.Click += new System.EventHandler(this.button10_Click);
            // 
            // sayi8button
            // 
            this.sayi8button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.sayi8button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sayi8button.ForeColor = System.Drawing.Color.Crimson;
            this.sayi8button.Location = new System.Drawing.Point(157, 294);
            this.sayi8button.Name = "sayi8button";
            this.sayi8button.Size = new System.Drawing.Size(75, 70);
            this.sayi8button.TabIndex = 12;
            this.sayi8button.Text = "8";
            this.sayi8button.UseVisualStyleBackColor = false;
            this.sayi8button.Click += new System.EventHandler(this.button11_Click);
            // 
            // sayi7button
            // 
            this.sayi7button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.sayi7button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sayi7button.ForeColor = System.Drawing.Color.Crimson;
            this.sayi7button.Location = new System.Drawing.Point(59, 294);
            this.sayi7button.Name = "sayi7button";
            this.sayi7button.Size = new System.Drawing.Size(75, 70);
            this.sayi7button.TabIndex = 11;
            this.sayi7button.Text = "7";
            this.sayi7button.UseVisualStyleBackColor = false;
            this.sayi7button.Click += new System.EventHandler(this.button12_Click);
            // 
            // sayibolubutton
            // 
            this.sayibolubutton.BackColor = System.Drawing.Color.LightCoral;
            this.sayibolubutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sayibolubutton.ForeColor = System.Drawing.Color.Crimson;
            this.sayibolubutton.Location = new System.Drawing.Point(346, 370);
            this.sayibolubutton.Name = "sayibolubutton";
            this.sayibolubutton.Size = new System.Drawing.Size(75, 70);
            this.sayibolubutton.TabIndex = 18;
            this.sayibolubutton.Text = "/";
            this.sayibolubutton.UseVisualStyleBackColor = false;
            // 
            // sayiesitbutton
            // 
            this.sayiesitbutton.BackColor = System.Drawing.Color.LemonChiffon;
            this.sayiesitbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sayiesitbutton.ForeColor = System.Drawing.Color.Crimson;
            this.sayiesitbutton.Location = new System.Drawing.Point(255, 370);
            this.sayiesitbutton.Name = "sayiesitbutton";
            this.sayiesitbutton.Size = new System.Drawing.Size(75, 70);
            this.sayiesitbutton.TabIndex = 17;
            this.sayiesitbutton.Text = "=";
            this.sayiesitbutton.UseVisualStyleBackColor = false;
            // 
            // sayi0button
            // 
            this.sayi0button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.sayi0button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sayi0button.ForeColor = System.Drawing.Color.Crimson;
            this.sayi0button.Location = new System.Drawing.Point(157, 370);
            this.sayi0button.Name = "sayi0button";
            this.sayi0button.Size = new System.Drawing.Size(75, 70);
            this.sayi0button.TabIndex = 16;
            this.sayi0button.Text = "0";
            this.sayi0button.UseVisualStyleBackColor = false;
            this.sayi0button.Click += new System.EventHandler(this.button15_Click);
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.Color.DodgerBlue;
            this.button16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button16.ForeColor = System.Drawing.Color.Crimson;
            this.button16.Location = new System.Drawing.Point(59, 370);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(75, 70);
            this.button16.TabIndex = 15;
            this.button16.Text = "C";
            this.button16.UseVisualStyleBackColor = false;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(29F, 56F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(480, 507);
            this.Controls.Add(this.sayibolubutton);
            this.Controls.Add(this.sayiesitbutton);
            this.Controls.Add(this.sayi0button);
            this.Controls.Add(this.button16);
            this.Controls.Add(this.sayicarpibutton);
            this.Controls.Add(this.sayi9button);
            this.Controls.Add(this.sayi8button);
            this.Controls.Add(this.sayi7button);
            this.Controls.Add(this.sayieksibutton);
            this.Controls.Add(this.sayi6button);
            this.Controls.Add(this.sayi5button);
            this.Controls.Add(this.sayi4button);
            this.Controls.Add(this.sayiartibutton);
            this.Controls.Add(this.sayi3button);
            this.Controls.Add(this.sayi2button);
            this.Controls.Add(this.sayi1button);
            this.Controls.Add(this.ekranLabel);
            this.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Margin = new System.Windows.Forms.Padding(9);
            this.Name = "Form1";
            this.Text = "Form1";
            this.TopMost = true;
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label ekranLabel;
        private System.Windows.Forms.Button sayi1button;
        private System.Windows.Forms.Button sayi2button;
        private System.Windows.Forms.Button sayi3button;
        private System.Windows.Forms.Button sayiartibutton;
        private System.Windows.Forms.Button sayieksibutton;
        private System.Windows.Forms.Button sayi6button;
        private System.Windows.Forms.Button sayi5button;
        private System.Windows.Forms.Button sayi4button;
        private System.Windows.Forms.Button sayicarpibutton;
        private System.Windows.Forms.Button sayi9button;
        private System.Windows.Forms.Button sayi8button;
        private System.Windows.Forms.Button sayi7button;
        private System.Windows.Forms.Button sayibolubutton;
        private System.Windows.Forms.Button sayiesitbutton;
        private System.Windows.Forms.Button sayi0button;
        private System.Windows.Forms.Button button16;
    }
}

